var o = {x:1,y:2,z:3}
'toString' in o;
// propertyIsEnumerable是否可枚举
o.propertyIsEnumerable('toString');//false
var key;
for(key in o){
	console.log(key)
}//1,2,3

var obj = Object.create(o)
obj.a = 4;
var key;
for(key in obj){
	console.log(key)
}//1,2,3,4


var obj = Object.create(o)
obj.a = 4;
var key;
for(key in obj){
	if(obj.hasOwnproperty(key)){
		console.log(key)//4
	}
}