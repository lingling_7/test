var man = {
	name:'zb',
	weixin:'zzb8999',
	get age(){
		return new Date().getFullYear()-1996
	},
	set age(val){
		console.log('Age can not be set to' +val)
	}
}
console.log(man.age)
man.age = 100;
console.log(man.age)

//当age有定义的时候

var man = {
	name:'ds',
	$age:null,
	get age(){
		if(this.$age == undefined){
			return new Date().getFullYear()-1996
		}
	},
	set age(val){
		val= +val//将val变成数字
		if (!isNaN(val) && val >0 && val<150){
			this.$age = +val;
		}else{
			throw new Error('error:' + val)
		}
	}
}
console.log(man.age);//22
man.age = 100;
console.log(man.age)//100

//get,set与原型链

function Foo(){}

Object.defineProperty(Foo.prototype,'z',{get:function(){return 1;}});
var boj = new Foo();
obj.z;//1
obj.z = 10;//无法赋值，因为原型链中默认，writeble为false
console.log(obj.z)// 1